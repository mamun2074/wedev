<?php
/**
 * Created by PhpStorm.
 * User: Mahmud
 * Date: 4/19/2020
 * Time: 4:31 PM
 */

namespace App;


class Model
{

//    DB Connection Info
    private $host = "";
    private $dbName = "";
    private $userName = "";
    private $password = "";

    public function __construct()
    {
        $this->setConnection();
    }

    public function setConnection()
    {
        $this->host = "localhost";
        $this->dbName = "weDev";
        $this->userName = "root";
        $this->password = "";
    }

    public  function connectDB()
    {
        return new \PDO('mysql:host=' . $this->host . ';dbname=' . $this->dbName . '', $this->userName, $this->password);
    }


    public static function showAll()
    {
        try {
            $pdo = (new Model())->connectDB();
            $query = "SELECT * FROM `todos`";
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetchAll( \PDO::FETCH_ASSOC);
            return $data;

        } catch (\PDOException $e) {
            echo 'Error' . $e->getMessage();
        }
    }



}