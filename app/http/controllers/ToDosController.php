<?php
/**
 * Created by PhpStorm.
 * User: Mahmud
 * Date: 4/19/2020
 * Time: 3:45 PM
 */

namespace App\http\controllers;

use App\ToDo;

class ToDosController
{

    public static function index()
    {
        $allTodo = ToDo::showAll();

        return json_encode($allTodo);
    }

    public static function store($todo)
    {
        $allTodo = ToDo::dataInsert($todo);
        return json_encode($allTodo);
    }

    public static function updateCompleted($checkedId, $isChecked)
    {

        $allTodo = ToDo::isCompleted($checkedId, $isChecked);
        return json_encode($allTodo);
    }

    public static function deleteItem($id)
    {
        $allTodo = ToDo::deleted($id);
        return json_encode($allTodo);
    }
    public static function deleteCompletedItem()
    {
        $allTodo = ToDo::completedItemDelete();
        return json_encode($allTodo);
    }

    public static function update($id,$content)
    {
        $allTodo = ToDo::updateToDo($id,$content);
        return json_encode($allTodo);
    }




}