<?php
/**
 * Created by PhpStorm.
 * User: Mahmud
 * Date: 4/19/2020
 * Time: 4:00 PM
 */

namespace App;

use App\Model;

class ToDo extends Model
{

    public static function dataInsert($data)
    {
        try {
            $pdo = (new Model())->connectDB();
            $query = "INSERT INTO `todos`(`content`) VALUES (:content)";

            $stmt = $pdo->prepare($query);
            $ss = $stmt->execute(array(
                ':content' => $data
            ));
            return ToDo::showAll();

        } catch (\PDOException $e) {
            echo 'Error' . $e->getMessage();
        }
    }

    public static function isCompleted($checkedId, $isChecked)
    {
        try {
            $pdo = (new Model())->connectDB();
            $query = "UPDATE `todos` SET active = :active WHERE id = :id";
            $stmt = $pdo->prepare($query);
            $stmt->execute(array(
                ':id' => $checkedId,
                ':active' => $isChecked,
            ));
            return ToDo::showAll();

        } catch (\PDOException $e) {
            echo 'Error' . $e->getMessage();
        }
    }

    public static function deleted($id)
    {
        try {
            $pdo = (new Model())->connectDB();
            $query = "DELETE FROM  `todos` WHERE id = :id";
            $stmt = $pdo->prepare($query);
            $stmt->execute(array(
                ':id' => $id,
            ));
            return ToDo::showAll();

        } catch (\PDOException $e) {
            echo 'Error' . $e->getMessage();
        }
    }

    public static function completedItemDelete()
    {
        try {
            $pdo = (new Model())->connectDB();
            $query = "DELETE FROM  `todos` WHERE active = :active";
            $stmt = $pdo->prepare($query);
            $stmt->execute(array(
                ':active' => 1,
            ));
            return ToDo::showAll();

        } catch (\PDOException $e) {
            echo 'Error' . $e->getMessage();
        }
    }

    public static function updateToDo($id, $todo)
    {
        try {
            $pdo = (new Model())->connectDB();
            $query = "UPDATE `todos` SET content = :content WHERE id = :id";
            $stmt = $pdo->prepare($query);
            $stmt->execute(array(
                ':id' => $id,
                ':content' => $todo,
            ));
            return ToDo::showAll();

        } catch (\PDOException $e) {
            echo 'Error' . $e->getMessage();
        }
    }


}