<?php
/**
 * Created by PhpStorm.
 * User: Mahmud
 * Date: 4/19/2020
 * Time: 3:49 PM
 */

?>



<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <!--  fontawesome-->
    <link rel="stylesheet" href="assets/css/all.css">

    <!--   custom style-->
    <link rel="stylesheet" href="assets/css/style.css">

    <!--    responsive design-->
    <link rel="stylesheet" href="assets/css/responsive.css">

    <title>WeDev-ToDo Assignment </title>
</head>
<body>

<div class="mid ">
    <div class="title">
        <h1 class="t-a-c">todos </h1>
    </div>

    <div class="input-area  box-shadow  mt-20">
        <input class="b-none" type="text" placeholder="What needs to be done?">
    </div>

    <div class=" list-area box-shadow">
        <div class="items">
            <ul>

            </ul>
        </div>

        <div id="itemDesc" class="row p-20">

        </div>
    </div>

</div>


<!--jquery-->
<script src="https://code.jquery.com/jquery-3.4.1.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
        crossorigin="anonymous"></script>

<!--fontawesome-->
<script src="assets/js/all.js"></script>

<!--custom Script-->
<script src="assets/js/script.js"></script>

<script>
    controller.init();
</script>
</body>
</html>







