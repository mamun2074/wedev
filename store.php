<?php
/**
 * Created by PhpStorm.
 * User: Mahmud
 * Date: 4/19/2020
 * Time: 5:53 PM
 *
 */

require_once 'vendor/autoload.php';

use App\http\controllers\ToDosController;


if ($_SERVER['REQUEST_METHOD']=='GET'){
    header('Location: http://localhost/weDev');
}

if ($_POST['submitType'] == 'all') {
    echo ToDosController::index();
} else if ($_POST['submitType'] == 'insert') {
    $todo = $_POST['todo'];
    echo ToDosController::store($todo);
} else if ($_POST['submitType'] == 'updateCompleted') {
    $checkedId = $_POST['checkedId'];
    $isChecked = $_POST['isChecked'];
    echo ToDosController::updateCompleted($checkedId, $isChecked);

} else if ($_POST['submitType'] == 'deleteItem') {
    $id = $_POST['id'];
    echo ToDosController::deleteItem($id);
} else if ($_POST['submitType'] == 'deleteCompletedItem') {
    echo ToDosController::deleteCompletedItem();

} else if ($_POST['submitType'] == 'updateToDo') {

    $id = $_POST['id'];
    $todo = $_POST['todo'];

    echo ToDosController::update($id,$todo);

}








