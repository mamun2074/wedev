let model = (() => {

    const baseUrl = 'http://localhost/weDev';
    let insertData = function (data) {
        let res = null;
        $.ajax({
            url: baseUrl + '/store.php',
            type: "POST",
            dataType: "json",
            async: false,
            data: {
                "submitType": 'insert',
                "todo": data
            },
            success: function (response) {
                res = response;
            },
            error: function (error) {
                console.log(error)
            }
        });
        return res;
    };
    let allData = function () {
        var data = null;
        $.ajax({
            url: baseUrl + '/store.php',
            type: "POST",
            dataType: "json",
            async: false,
            data: {
                submitType: "all",
            },
            success: function (response) {
                data = response;
            },
            error: function (err) {
                console.log(err);
            }
        });
        return data;
    };
    let updateIsCompleted = function (info) {
        var data = null;
        $.ajax({
            url: baseUrl + '/store.php',
            type: "POST",
            dataType: "json",
            async: false,
            data: {
                submitType: "updateCompleted",
                checkedId: info.checkedId,
                isChecked: info.isChecked,
            },
            success: function (response) {
                data = response;
            },
            error: function (err) {
                console.log(err);
            }
        });
        return data;
    };

    let deleteItem = function (id) {

        var data = null;
        $.ajax({
            url: baseUrl + '/store.php',
            type: "POST",
            dataType: "json",
            async: false,
            data: {
                submitType: "deleteItem",
                id: id,
            },
            success: function (response) {
                data = response;
            },
            error: function (err) {
                console.log(err);
            }
        });
        return data;
    };
    let deleteCompletedItem = function () {

        var data = null;
        $.ajax({
            url: baseUrl + '/store.php',
            type: "POST",
            dataType: "json",
            async: false,
            data: {
                submitType: "deleteCompletedItem"
            },
            success: function (response) {
                data = response;
            },
            error: function (err) {
                console.log(err);
            }
        });
        return data;
    };

    let updateToDo = function (id, todo) {

        var data = null;
        $.ajax({
            url: baseUrl + '/store.php',
            type: "POST",
            dataType: "json",
            async: false,
            data: {
                submitType: "updateToDo",
                id: id,
                todo: todo,

            },
            success: function (response) {
                data = response;

            },
            error: function (err) {
                console.log(err);
            }
        });
        return data;
    };


    return {
        getAllData: function () {
            return allData();
        },
        storeData: function (data) {
            return insertData(data);
        },
        updateComplete: function (info) {
            return updateIsCompleted(info);
        },
        itemDeleted: function (id) {
            return deleteItem(id);
        },
        clearCompleteItem: function () {
            return deleteCompletedItem();
        },
        todUpdate: function (id, todo) {
            return updateToDo(id, todo);
        }
    }
})();


let view = (() => {
    // Some Code

    let DOMString = {
        todo: '.input-area input',
        itemsUl: '.items ul',
        itemDesc: 'itemDesc',
        inputCheck: "input[type=checkbox]"
    };
    let createMenu = function (data, type) {

        var all = "all", active = "activeMenu", completed = "completedMenu";
        if (type == 'all') {
            all = 'active all';
        }
        if (type == 'active') {
            active = 'active activeMenu';
        }
        if (type == 'completed') {
            completed = 'active completedMenu';
        }


        let desc = `
                        <div class="clm-20">
                            <div class="total-item ">
                                <p>${data} item Left</p>
                            </div>
                        </div>
                        <div class="clm-60">
                            <div class="list-area-footer ">
                                <ul>
                                    <li>
                                        <a class="${all}" href="">All</a>
                                    </li>
                                    <li>
                                        <a class="${active}" href="">Active</a>
                                    </li>
                                    <li>
                                        <a class="${completed}" href="">Completed</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="clm-20 ">
                            <div class="clear-completed ">
                                <a href="">Clear Completed</a>
                            </div>
                        </div>`;

        return desc;
    };

    let createItem = function (data) {
        var spanTag, inputTag;
        if (data.active == 1) {
            spanTag = `<span style="color: rgba(0, 0, 0, .2);"><del>${data.content}</del>  </span>`;
            inputTag = `<input type="checkbox" checked>`;
        } else {
            spanTag = `<span> ${data.content} </span>`;
            inputTag = `<input type="checkbox">`;
        }

        let item = `
                <li id="${data.id}" class="items_id">
                    <div class="row">
                        <div class="clm-5">
                            <div class="checkbox ">
                                ${inputTag}
                            </div>
                        </div>
                        <div class="clm-90">
                            <div class="edit_item ">
                                ${spanTag}
                                <input  type="hidden" value="${data.content}">
                            </div>
                        </div>
                        <div class="clm-5">
                            <div class="cross ">
                                <i class="fas fa-times "></i>
                            </div>
                        </div>
                    </div>
                </li>`;
        return item;
    };
    return {
        getDOMString: () => DOMString,
        getElement: function () {
            return {
                getTodo: document.querySelector(DOMString.todo),
                getItemsUl: document.querySelector(DOMString.itemsUl),
                getItemDesc: document.getElementById(DOMString.itemDesc),
                getInputCheck: document.querySelectorAll("input[type='checkbox']"),
            };
        },
        itemsType: function (data, type) {
            let element = this.getElement();
            var items = "<span></span>";
            var total = 0;
            for (var key in data) {
                // Item Create li
                if (type == 'active') {
                    if (data[key].active == 0) {
                        items += createItem(data[key]);
                    }

                } else if (type == 'completed') {
                    if (data[key].active == 1) {
                        items += createItem(data[key]);
                    }
                } else if (type == 'all') {
                    items += createItem(data[key]);
                }
                if (data[key].active == 0) {
                    total++;
                }
            }
            // items create
            element.getItemsUl.innerHTML = items;
            // Menu update

            element.getItemDesc.innerHTML = createMenu(total, type);

            if (Object.keys(data).length == 0) {
                element.getItemDesc.parentElement.style.display = 'none';
            } else {
                element.getItemDesc.parentElement.style.display = 'block';
            }
            if (Object.keys(data).length != total) {
                element.getItemDesc.querySelector('.clear-completed').style.display = 'block';
            } else {
                element.getItemDesc.querySelector('.clear-completed').style.display = 'none';
            }
        },

        getActiveMenu: function (e) {
            // Searching Active Menu
            var listAreaElement = e.target.closest('.list-area');
            var activeMenuName = listAreaElement.querySelector('.active').textContent;

            var active = "";
            if (activeMenuName == 'All') {
                active = 'all'
            }
            if (activeMenuName == 'Active') {
                active = 'active'
            }

            if (activeMenuName == 'Completed') {
                active = 'completed'
            }
            return active;

        },

    };

})();


let controller = ((model, view) => {
    let element;
    element = view.getElement();

    let EventListner = function () {
        element.getTodo.addEventListener('keydown', insertInputData);

        setInterval(() => {
            afterGettingDataEvent();
        }, 100);

    };

    let afterGettingDataEvent = function () {

        // Completed Event
        var inputs = document.querySelectorAll("input[type='checkbox']");
        for (var i = 0; i < inputs.length; i++) {
            inputs[i].addEventListener('click', competedItem);
        }

        // Delete Event
        var cross = document.querySelectorAll(".cross");
        for (var index = 0; index < cross.length; index++) {
            cross[index].addEventListener('click', deleteItem);
        }

        // All Event
        let allMenu = document.querySelector('.all');
        allMenu.addEventListener('click', allItem);

        // Active Event
        let activeMenu = document.querySelector('.activeMenu');
        activeMenu.addEventListener('click', activeItem);

        // Completed Event
        let completedMenu = document.querySelector('.completedMenu');
        completedMenu.addEventListener('click', completedItem);

        // Clear Completed
        let clearCompleted = document.querySelector('.clear-completed a');
        clearCompleted.addEventListener('click', clearCompletdItem);

        // todo edit event

        let editContent = document.querySelectorAll('.edit_item ');
        for (let i = 0; i < editContent.length; i++) {
            editContent[i].addEventListener('dblclick', editToDo);
        }

    };

    let editToDo = (e) => {

        let parentEditItem = e.target.closest('.items_id');
        let isDel = parentEditItem.querySelector('.edit_item del');

        if (isDel == null) {

            parentEditItem.querySelector('.cross').style.display = 'none';
            parentEditItem.querySelector('.checkbox ').style.display = 'none';
            parentEditItem.querySelector('span').style.display = 'none';
            parentEditItem.querySelector('input[type=hidden]').type = 'text';

            let inputText = parentEditItem.querySelector('input[type=text]');

            inputText.focus();

            // After Focus
            var length = inputText.value.length;
            inputText.setSelectionRange(length, length);


            // click outside of input text
            inputText.addEventListener('focusout', function (e) {

                // No update check
                if (e.target.value == "") {
                    // Show New Items
                    view.itemsType(model.getAllData(), 'all');
                } else {

                    // Update New Item
                    let data = model.todUpdate(parentEditItem.id, e.target.value);

                    // Show New Items
                    view.itemsType(data, 'all');

                }


            });
        }
    };

    let allItem = (e) => {
        e.preventDefault();
        view.itemsType(model.getAllData(), 'all');
    };

    let clearCompletdItem = (e) => {
        e.preventDefault();

        // get Active Menu
        var activeMenue = view.getActiveMenu(e);

        // completed item delete and fetch other data
        var data = model.clearCompleteItem();

        // show current data
        view.itemsType(data, activeMenue);


    };

    let completedItem = (e) => {
        e.preventDefault();
        view.itemsType(model.getAllData(), 'completed');
    };
    let activeItem = (e) => {
        e.preventDefault();
        view.itemsType(model.getAllData(), 'active');

    };


    let deleteItem = (e) => {

        // target id
        var item_id = e.target.closest('.items_id').id;

        // item deleted
        var allData = model.itemDeleted(item_id);


        // Searching Active Menu
        var listAreaElement = e.target.closest('.list-area');
        var activeMenuName = listAreaElement.querySelector('.active').textContent;

        var active = "";
        if (activeMenuName == 'All') {
            active = 'all'
        }
        if (activeMenuName == 'Active') {
            active = 'active'
        }

        if (activeMenuName == 'Completed') {
            active = 'completed'
        }

        // updated all data
        view.itemsType(allData, active);


    };

    let competedItem = (e) => {

        // make checked input unchecked and unchecked input checked;
        var isChecked;
        if (e.target.checked) {
            isChecked = 1;
        } else {
            isChecked = 0;
        }
        // checked Id
        var checkedId = e.target.parentElement.parentElement.parentElement.parentElement.id;

        // Update Compelted or Uncompelte
        let info = {
            checkedId: checkedId,
            isChecked: isChecked
        };
        var data = model.updateComplete(info);


        var active = view.getActiveMenu(e);

        // view all updated info
        view.itemsType(data, active);

    };

    let insertInputData = (e) => {

        if (e.code == 'Enter' || e.code == 'NumpadEnter') {

            // get input data
            let content = e.target.value;

            if (content != "") {

                // Insert data and get new all data
                var allData = model.storeData(content);

                // after inserting input field turn into empty
                e.target.value = "";

                // Menu Set
                var activeMenuName = e.target.parentElement.parentElement.querySelector('.active ').textContent;
                var active = "all";
                if (activeMenuName == 'all') {
                    active = 'all';
                }
                if (activeMenuName == 'Active') {
                    active = 'active';
                }
                if (activeMenuName == 'Completed') {
                    active = 'completed';
                }

                // Show All data into dom
                view.itemsType(allData, active);

            }
        }

    };

    return {
        init: () => {
            console.log("Apps Running...");
            EventListner();
            view.itemsType(model.getAllData(), 'all');

        }
    }


})(model, view);
